package com.decamp.task58a30.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.decamp.task58a30.models.Customer;
import com.decamp.task58a30.repository.CustomerRepository;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired 
    CustomerRepository customerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getCustomer(){
        try {
        List<Customer> listCustomers = new ArrayList<>();
        customerRepository.findAll().forEach(listCustomers::add);

       return new ResponseEntity<>(listCustomers, HttpStatus.OK);
            
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }
}
