package com.decamp.task58a30.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.decamp.task58a30.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    
}
