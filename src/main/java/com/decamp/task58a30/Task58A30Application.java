package com.decamp.task58a30;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task58A30Application {

	public static void main(String[] args) {
		SpringApplication.run(Task58A30Application.class, args);
	}

}
